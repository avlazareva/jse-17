package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.api.repository.ITaskRepository;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAll(Sort sort);

}