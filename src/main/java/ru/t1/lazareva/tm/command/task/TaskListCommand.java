package ru.t1.lazareva.tm.command.task;

import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.model.Task;
import ru.t1.lazareva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    private static final String NAME = "task-list";

    private static final String DESCRIPTION = "Show task list.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(sort);
        renderTasks(tasks);
    }

}