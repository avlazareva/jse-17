package ru.t1.lazareva.tm.command.task;

import ru.t1.lazareva.tm.model.Task;
import ru.t1.lazareva.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    private static final String NAME = "task-create";

    private static final String DESCRIPTION = "Create new task.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASKS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = getTaskService().create(name, description);
    }

}