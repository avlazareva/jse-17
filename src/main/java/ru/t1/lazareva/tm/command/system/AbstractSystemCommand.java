package ru.t1.lazareva.tm.command.system;

import ru.t1.lazareva.tm.api.service.ICommandService;
import ru.t1.lazareva.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}