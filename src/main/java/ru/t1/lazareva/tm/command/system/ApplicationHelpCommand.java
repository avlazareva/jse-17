package ru.t1.lazareva.tm.command.system;

import ru.t1.lazareva.tm.api.model.ICommand;
import ru.t1.lazareva.tm.api.service.ICommandService;
import ru.t1.lazareva.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    private static final String NAME = "help";

    private static final String DESCRIPTION = "Show command list.";

    private static final String ARGUMENT = "-h";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) System.out.println(command);
    }

}
